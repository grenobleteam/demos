/**
 * Created by loic on 25/07/15.
 */

var generators = require('yeoman-generator');
var request = require('request');
var Q = require('q');
var _ = require('lodash');
var FS = require('fs');
var slug = require('slug');
var env = require('node-env-file');

if (!process.env.USERNAME || !process.env.PASSWORD) {
    env(__dirname + '/../../.env');
}

var user = process.env.USERNAME || '';
var password = process.env.PASSWORD || '';

var jiraWrapper = require('../../lib/jira-wrapper')(user, password);

module.exports = generators.Base.extend({
    _demoNumber: null,
    _epicsCollection: null,
    _demoDate: null,
    _stories: null,

    _retrieveEpics: function () {
        var sprint = this._demoNumber;

        return jiraWrapper.retrieveStoriesFromSprint(sprint).then(function (response) {
            if (!response.issues) {
                throw new Error('Sprint ' + sprint + ' does not exist');
            }

            this._stories = response.issues.reduce(function(prev, current) {
                prev[current.fields.customfield_10007] = prev[current.fields.customfield_10007] || [];
                prev[current.fields.customfield_10007].push({id: current.key, status: slug(current.fields.status.name)});
                return prev;
            }, {});

            return jiraWrapper.retrieveEpicsFromStories(response.issues);
        }.bind(this));
    },

    _retrieveDefaultDemoNumber: function () {
        var d = Q.defer();

        FS.readdir(this.destinationRoot(), function (err, files) {
            if (err) d.reject();

            var max = files.reduce(function (previous, current) {
                var res = current.match(/^demo-media-(\d+)-slides$/);

                if (res && res.length && parseInt(res[1], 10) > previous) {
                    return parseInt(res[1], 10);
                }
                return previous;
            }, 0);

            d.resolve(max + 1);
        });

        return d.promise;
    },

    _createSlidesDirectory: function (answers) {
        this._demoNumber = answers.demoNumber;
        this._demoDate = answers.date;

        this.destinationRoot('demo-media-' + this._demoNumber + '-slides');

        this.fs.copyTpl(this.templatePath('next_demo.html'), this.destinationPath('next_demo.html'));
        this.fs.copyTpl(this.templatePath('roi.html'), this.destinationPath('roi.html'));
        return this._retrieveEpics();
    },

    _askDemoNumberAndDate: function (demoNumber) {
        return this._prompt([{
            type: 'input',
            name: 'demoNumber',
            message: 'Your demo number',
            default: demoNumber
        },{
            type: 'input',
            name: 'date',
            message: 'What will be the demo date ? (YYYY-MM-DD)'
        }]);
    },

    _askWhichEpics: function (epics) {
        this._epicsCollection = epics;

        var choices = Object.keys(epics).map(function (epicId) {
            return {
                name: ' ' + epics[epicId].name,
                value: epicId,
                checked: true
            };
        });
        return this._prompt({
            type: 'checkbox',
            message: 'Choose epics',
            name: 'epics',
            choices: choices
        });
    },

    _prompt: function (params) {
        var d = Q.defer();

        this.prompt(params, function (answers) {
            d.resolve(answers);
        });
        return d.promise;
    },

    _createIndexAndEpics: function(epicsAnswer) {
        var chosenEpics = epicsAnswer.epics;

        chosenEpics.forEach(function (epicId) {
            this.fs.copyTpl(
                this.templatePath('epicIndex.html'),
                this.destinationPath(this._epicsCollection[epicId].slug + '/index.html'),
                {
                    epicName: this._epicsCollection[epicId].name,
                    stories: this._stories[epicId]
                }
            );
        }, this);

        this.fs.copyTpl(
            this.templatePath('index.html'),
            this.destinationPath('index.html'),
            {
                demoNumber: this._demoNumber,
                date: this._demoDate,
                epics: chosenEpics.map(function (epicId) {
                    return { id: epicId, slug: this._epicsCollection[epicId].slug}
                }, this)
            }
        );
    },

    prompting: function () {
        this.destinationRoot('demo_files');
        this.sourceRoot('../generators/app/templates');
        var done = this.async();

        this._retrieveDefaultDemoNumber()
            .then(this._askDemoNumberAndDate.bind(this))
            .then(this._createSlidesDirectory.bind(this))
            .then(this._askWhichEpics.bind(this))
            .then(this._createIndexAndEpics.bind(this))
            .fail(function (err) {
                console.log(err);
            })
            .finally(function () {
            done();
        });
    }
});