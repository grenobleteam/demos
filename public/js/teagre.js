// var sprintLabel = 'MediaSprints Sprint';
var sprintLabel = 'ForumSquad Sprint';

/*
    TITLE & FOOTER
 */
(function() {
    $('.slides > section:nth-child(1) h1').text('Demo ' + demoNb);
/*    document.title = 'Grenoble Team - Demo ' + demoNb;*/
    var str = /*demoDate + " / " +*/ document.title;
    document.styleSheets[0].insertRule('.slides section:not(.stack):before { content: "' + str + '" !important; }', document.styleSheets[0].cssRules.length);
}());

/*
    JIRA LINKS
 */
(function() {
    var storiesElem = document.querySelectorAll('.jira-story');
    var linkToRequest = {};

    for (var i = 0; i < storiesElem.length; i++) {
        var $storyElem = $(storiesElem[i]);

        var label = $storyElem.text().replace('#', '');
        var url = 'https://purch1.atlassian.net/browse/' + label;
        var $link = $('<a href="' + url + '" target="_blank">' + label + '</a>');
        $storyElem.before($link);
        $storyElem.remove();

        if ($storyElem.hasClass('with-title')) {
            if (!linkToRequest[label]) {
                linkToRequest[label] = $link;
            } else {
                linkToRequest[label] = linkToRequest[label].add($link);
            }
        }
    }

    if (Object.keys(linkToRequest).length > 0) {
        $.ajax({
            url: '/stories',
            data: {
                jql: 'key = ' + Object.keys(linkToRequest).join(' OR key = '),
                fields: 'summary',
                maxResults: 500
            },
            dataType: 'json',
            success: function(result) {
                for (var i = 0; i < result.issues.length; i++) {
                    var storyInfo = result.issues[i];

                    linkToRequest[storyInfo.key].text(storyInfo.key + ' - ' + storyInfo.fields.summary);
                }
            },
            error: function() {
                console.log('error', arguments);
            }
        })
    }
}());

/*
    Epic Pie Chart
 */
(function() {
    var pieChartId = "toc";

    if (document.getElementById("toc")) {
        function updateLumnisoty(rgb, luminosity) {
            var color = Raphael.rgb2hsl(rgb.r, rgb.g, rgb.b);

            color.l = luminosity;

            return color;
        }

        Raphael.fn.pieChart = function (cx, cy, r, values, labels, stroke) {
            var paper = this,
                rad = Math.PI / 180,
                chart = this.set();
            function sector(cx, cy, r, startAngle, endAngle, params) {
                var x1 = cx + r * Math.cos(-startAngle * rad),
                    x2 = cx + r * Math.cos(-endAngle * rad),
                    y1 = cy + r * Math.sin(-startAngle * rad),
                    y2 = cy + r * Math.sin(-endAngle * rad);
                return paper.path(["M", cx, cy, "L", x1, y1, "A", r, r, 0, +(endAngle - startAngle > 180), 0, x2, y2, "z"]).attr(params);
            }
            function jumpToHash() {
                window.location.hash = '/' + this.data('key');
            }
            var angle = 0,
                total = 0,
                start = 0.6,
                process = function (j) {
                    if (!values[j]) {
                        return;
                    }

                    var value = values[j],
                        angleplus = 360 * value / total,
                        popangle = angle + (angleplus / 2),
                        color = Raphael.hsb(start, .75, 1),
                        ms = 500,
                        delta = 50,
                        bcolor = Raphael.hsb(start, 1, 1),
                        p = sector(cx, cy, r, angle, angle + angleplus, {fill: "90-" + bcolor + "-" + color, stroke: stroke, "stroke-width": 3, cursor: 'pointer'})
                            .data('key', labels[j].key)
                            .click(jumpToHash),
                        txt = paper
                            .text(cx + (r + delta + 55) * Math.cos(-popangle * rad), cy + (r + delta + 25) * Math.sin(-popangle * rad), labels[j].label)
                            .attr({fill: updateLumnisoty(Raphael.getRGB(bcolor), .3), stroke: "none", cursor: 'pointer', "font-size": 20 }).data('key', labels[j].key)
                            .click(jumpToHash);

                    var rectBox = txt.getBBox();
                    var rect = paper.rect(rectBox.x - 5, rectBox.y, rectBox.width + 10, rectBox.height, 5);
                    rect.attr({fill: updateLumnisoty(Raphael.getRGB(bcolor), .9), stroke: updateLumnisoty(Raphael.getRGB(bcolor), .3), borderRadius: .5});
                    p.mouseover(function () {
                        p.stop().animate({transform: "s1.1 1.1 " + cx + " " + cy}, ms, "elastic");
                    }).mouseout(function () {
                        p.stop().animate({transform: ""}, ms, "elastic");
                    });
                    angle += angleplus;
                    rect.insertBefore(txt);
                    chart.push(p);
                    chart.push(rect);
                    chart.push(txt);
                    rect.insertBefore(txt);
                    start += .1;
                };
            for (var i = 0, ii = values.length; i < ii; i++) {
                total += values[i];
            }
            for (i = 0; i < ii; i++) {
                process(i);
            }
            return chart;
        };

        // customfield_10007 : epic ID
        // customfield_10004 : estimate
        $.ajax({
            url: '/stories',
            data: {
                jql: 'Sprint = "' + sprintLabel + ' ' + demoNb + '" and type != "Task" and type != "Sub-task" and ("labels" = teagre or project = GRE)',
                fields: 'customfield_10007,customfield_10004',
                maxResults: 200
            },
            success: function(response) {
                var epics = {};

                for (var i = 0; i < response.issues.length; i++) {
                    var issue = response.issues[i];
                    var epicId = issue.fields.customfield_10007;

                    if (epicId !== null) {
                        epics[epicId] = epics[epicId] || 0;
                        epics[epicId] += issue.fields.customfield_10004;
                    }
                }

                $.ajax({
                    url: '/stories',
                    data: {
                        jql: 'key = ' + Object.keys(epics).join(' OR key = '),
                        fields: 'customfield_10008'
                    },
                    dataType: 'json',
                    success: function(result) {
                        var labels = [];
                        var values = [];

                        for (var i = 0; i < result.issues.length; i++) {
                            var storyInfo = result.issues[i];
                            var epicId = storyInfo.key;

                            var epic = {
                                key: epicId,
                                label: storyInfo.fields.customfield_10008
                            };

                            labels.push(epic);
                            values.push(epics[epicId]);
                        }

                        labels.sort(function(epic1, epic2) {
                            return parseInt(epics[epic2.key], 10) - parseInt(epics[epic1.key], 10);
                        });

                        values.sort(function(a, b) {
                            return parseInt(b, 10) - parseInt(a, 10);
                        });

                        Raphael(pieChartId, '90%', 700).pieChart(350, 350, 200, values, labels, "#fff");
                    },
                    error: function() {
                        console.log('error', arguments);
                    }
                })
            },
            error: function() {
                console.log('error', arguments);
            }
        });
    }
}());

/* Debug mode */
(function() {
    if (!window.location.search) {
        return;
    }

    var urlParams = window.location.search.substr(1).split('&');
    var params = {};
    $(urlParams).each(function(i, keyValue) {
        var param = keyValue.split('=');
        if (param[1]) {
            params[param[0]] = param[1];
        } else {
            params[param[0]] = true;
        }
    });

    if (typeof params['debug'] === 'undefined') {
        return;
    }

    var sprintFilter = 'Sprint = "' + sprintLabel + ' ' + demoNb + '"';
    if (typeof params['debug'] === 'string') {
        var sprints = params['debug'].split(',').map(function(sprintNb) { return '"' + sprintNb + '"'; });

        sprintFilter = '(Sprint = "' + sprintLabel + ' ' + sprints.join('" OR Sprint = "') + '")';
    }

    // customfield_10007 : epic ID
    // customfield_10004 : estimate
    $.ajax({
        url: '/stories',
        data: {
            jql: sprintFilter + ' and type != "Task" and type != "Sub-task" and (project = GRE)',
            fields: 'status,customfield_10007,summary',
            maxResults: 200
        },
        success: function(response) {
            var epics = [];

            for (var i = 0; i < response.issues.length; i++) {
                var issue = response.issues[i];
                var epicId = issue.fields.customfield_10007;
                var story = {
                    label: issue.fields.summary,
                    status: issue.fields.status.name,
                    id: issue.key
                }

                if (epicId !== null) {
                    epics[epicId] = epics[epicId] || {};
                    epics[epicId].issues = epics[epicId].issues || {};
                    epics[epicId].issues[story.status] = epics[epicId].issues[story.status] || [];
                    epics[epicId].issues[story.status].push(story);
                }
            }

            $.ajax({
                url: '/stories',
                data: {
                    jql: 'key = ' + Object.keys(epics).join(' OR key = '),
                    fields: 'customfield_10008'
                },
                dataType: 'json',
                success: function(result) {
                    var labels = [];
                    var values = [];

                    for (var i = 0; i < result.issues.length; i++) {
                        var storyInfo = result.issues[i];
                        var epicId = storyInfo.key;

                        epics[epicId].label = storyInfo.fields.customfield_10008;
                    }

                    $(Object.keys(epics)).each(function(i, epicId) {
                        console.log("%cEPIC %s - %s", 'font-weight: bold; font-size: larger;', epicId, epics[epicId].label);

                        var status = 'Done';
                        if (typeof epics[epicId].issues[status] !== 'undefined' && epics[epicId].issues[status].length > 0) {
                            $(epics[epicId].issues[status]).each(function(j, story) {
                                console.log('%c[%s] %s - %s', 'color: green;', status, story.id, story.label)
                            });
                        }

                        var status = 'In Progress';
                        if (typeof epics[epicId].issues[status] !== 'undefined' && epics[epicId].issues[status].length > 0) {
                            $(epics[epicId].issues[status]).each(function(j, story) {
                                console.log('%c[%s] %s - %s', 'color: darkorange;', status, story.id, story.label)
                            });
                        }

                        var status = 'To Do';
                        if (typeof epics[epicId].issues[status] !== 'undefined' && epics[epicId].issues[status].length > 0) {
                            $(epics[epicId].issues[status]).each(function(j, story) {
                                console.log('%c[%s] %s - %s', 'color: red;', status, story.id, story.label)
                            });
                        }

                        console.log('');
                    });
                },
                error: function() {
                    console.log('error', arguments);
                }
            })
        },
        error: function() {
            console.log('error', arguments);
        }
    });    
}());

(function() {
    $('.image-wrap a').each(function(i, link) {
        $(link).append('<img class="big" src="' + $(link).attr('href') + '">');
    });
    $('.external-link').each(function(i, link) {
        $(link).attr('target', '_blank');
    })
    $('.image-wrap a').click(function(e) {
        e.preventDefault();

        var $img = $(this).find('.big');

        var target = $img.get(0);

        if (!document.fullscreenElement && !document.mozFullScreenElement && !document.webkitFullscreenElement) {
            if (target.requestFullscreen) {
                target.requestFullscreen();
            } else if (target.mozRequestFullScreen) {
                target.mozRequestFullScreen();
            } else if (target.webkitRequestFullscreen) {
                target.webkitRequestFullscreen();
            } else if (target.msRequestFullscreen) {
                target.msRequestFullscreen();
            }
        } else {
            if (document.cancelFullScreen) {
                document.cancelFullScreen();
            } else if (document.mozCancelFullScreen) {
                document.mozCancelFullScreen();
            } else if (document.webkitCancelFullScreen) {
                document.webkitCancelFullScreen();
            }
        }

        return false;
    })
}());
