# Grenoble Team Demos #

[![Last deploy on Heroku](http://demo-teagre.herokuapp.com/last-deploy)](http://demo-teagre.herokuapp.com)

Application runs here: https://demo-teagre.herokuapp.com

## Run locally ##

### Clone project ###
```
#!bash
git clone git@bitbucket.org:grenobleteam/demos.git && cd demos
npm install
```

### Create configuration file ###

Create a `.env` file in the root of the project with your Jira login & password:
```
USERNAME=lgiraudel
PASSWORD=bliblablou
```

### Run app ###
```
#!bash
node app.js
```

Now you can check the app at http://localhost:8080

## Update heroku instance ##

### Install Heroku Toolbet ###

```
#!bash
wget -qO- https://toolbelt.heroku.com/install-ubuntu.sh | sh
```

### Conigure heroku ###

```
#!bash
heroku login
```

Then ask @lgiraudel an access to the Heroku Teagre-Demo app.

### Clone project & edit project ###
```
#!bash
git clone git@bitbucket.org:grenobleteam/demos.git && cd demos
git remote add heroku https://git.heroku.com/demo-teagre.git
[ create commits ]
git push heroku master
```

If needed, you can set config variables (like Jira user / password) with the following command:
```
#!bash
heroku config:set USERNAME=lgiraudel PASSWORD=bliblablou
```



## Tips ##

### Link to Jira story ###

To create a link to a Jira story just add the following markup:

```
#!html
<span class="jira-story">#GRE-1234</span>
```

This will produce this:
```
#!html
<a href="https://purch1.atlassian.net/browse/GRE-1234">GRE-1234</a>
```

If you want to inject the story title, just add the `with-title` class:
```
#!html
<span class="jira-story with-title">#GRE-1234</span>
```

This will produce this:
```
#!html
<a href="https://purch1.atlassian.net/browse/GRE-1234">GRE-1234 - Title of your story</a>
```

### Progress bar for epic stories ###

If you want to display a progress bar to show done stories ratio of an epic, you can use this piece of code:

```
#!html
<div class="progress-bar" data-epic="GRE-1556"></div>
```

## Todo ##

* Simplified markup
* Auto-generated story list for a specific epic
* Content of a slide generated from comments in Jira
* ...

## Done ##
* ~~Auto-generated progress bar~~
* ~~Pie charts of work done on epics~~
* ~~Directory to contain all demos~~
* ~~Deploy in Heroku~~
* ~~Redirect "/" to the last created demo~~
* ~~Use templates to explode slides~~
* ~~Fix link to old demo~~