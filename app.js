var express = require('express');
var request = require('request');
var fs      = require('fs');
var nunjucks = require('nunjucks');
var moment  = require('moment');
var env = require('node-env-file');

if (!process.env.USERNAME || !process.env.PASSWORD) {
    env(__dirname + '/.env');
}

var user = process.env.USERNAME || '';
var password = process.env.PASSWORD || '';
var jiraWrapper = require('./lib/jira-wrapper')(user, password);

var port = process.env.PORT || 8080;

var app = express();
app.use(express.static('public'));

nunjucks.configure('demo_files', {
    autoescape: true,
    express: app,
    watch: true
});
app.set('views', __dirname + '/demo_files');
app.set('view engine', 'html');

app.get('/', function (req, res) {
    fs.readdir(__dirname + '/demo_files', function (err, files) {
        if (err) throw err;

        var max = files.reduce(function (previous, current) {
            var res = current.match(/^demo-media-(\d+)-slides$/);

            if (res && res.length && parseInt(res[1], 10) > previous) {
                return parseInt(res[1], 10);
            }
            return previous;
        }, 0);

        res.render('demo-media-' + max + '-slides/index.html');
    })
});

app.get('/demo:number([0-9]+).html', function (req, res) {
    res.redirect('/demo' + req.params.number);
});

app.get('/demo:number([0-9]+)', function(req, res) {
    var debug = typeof req.query.debug !== 'undefined' ? 'debug' : '';
    res.render('demo-media-' + req.params.number + '-slides/index.html', { debug: debug });
});

app.get('/v2/:number([0-9]+)', function(req, res) {
    var debug = typeof req.query.debug !== 'undefined';
    var sprint = req.params.number;
    jiraWrapper.retrieveStoriesFromSprint(sprint)
    .then(function(res) {
        var stories = filterStories(res.issues, debug);
        return storiesToEpicsStructure(stories);
    })
    .then(getEpicsTitles)
    .then(enrichStoriesWithComments)
    .then(function(epics) {
        res.render('v2.html', { epics: epics, sprint: sprint });
    });
});

app.get('/stories', function(req, res) {
    var url = 'https://purch1.atlassian.net/rest/api/2/search';

    request({
        url: url,
        qs: req.query,
        auth: {
            user: user,
            password: password
        }
    }, function(err, response, body) {
        if (err) throw err;

        res.type('json');
        if (response.statusCode === 200) {
            res.write(body);
        }
        res.status(response.statusCode);
        res.end();
    });
});

app.get('/last-deploy', function(req, res) {
    var herokuToken = 'df87d380-20fb-41cd-9664-7d79949e7071';
    var herokuApiUrl = 'https://api.heroku.com/apps/demo-teagre';

    request({
        url: herokuApiUrl,
        headers: {
            'Accept': 'application/vnd.heroku+json; version=3',
            'Authorization': 'Bearer ' + herokuToken
        }
    }, function(err, response, body) {
        if (err) throw err;
        var json = JSON.parse(body);

        res.redirect('https://img.shields.io/badge/Last%20deploy-' + moment(json.updated_at).fromNow() + '-6762A6.svg')
    });
});

function filterStories(stories, debug) {
    return stories.filter(function(story) {
        if (debug) {
            if (story.fields.status.name !== 'Done' && story.fields.status.name !== 'In Progress') {
                return false;
            }
        } else {
            if (story.fields.status.name !== 'Done') {
                return false;
            }
        }
        return true;
    });
}

function storiesToEpicsStructure(stories) {
    return stories.reduce(function(epics, story) {
        if (story.fields.customfield_10007) {
            epics[story.fields.customfield_10007] = epics[story.fields.customfield_10007] || {
                key: story.fields.customfield_10007,
                stories: {}
            };

            epics[story.fields.customfield_10007].stories[story.key] = {
                key: story.key,
                title: story.fields.summary
            };
        }

        return epics;
    }, {});
}

function getEpicsTitles(epics) {
    return jiraWrapper.retrieveStories(Object.keys(epics)).then(function(stories) {
        epics = Object.keys(stories).reduce(function(epics, storyId) {
            var story = stories[storyId];

            epics[story.key].title = story.fields.summary;

            return epics;
        }, epics);

        return epics;
    });
}

function associateCommentsToStories(commentCollections, storyKeys, epics) {
    var comments = commentCollections.map(function(comments) {
        if (comments.length == 0) {
            return null;
        }

        var demoComments = comments.reduce(function(comments, comment) {
            if (comment.body.indexOf('{demo}') !== -1 || comment.body.indexOf('#demo') !== -1) {
                comments.push(comment);
            }
            return comments;
        }, []);

        if (demoComments.length == 0) {
            return null;
        }

        return demoComments[demoComments.length - 1];
    });

    var commentsPerStory = storyKeys.reduce(function(commentsPerStory, storyKey, index) {
        commentsPerStory[storyKey] = comments[index] ? comments[index].renderedBody : null;

        return commentsPerStory;
    }, {});

    Object.keys(epics).forEach(function(epicKey) {
        var epic = epics[epicKey];

        Object.keys(epic.stories).forEach(function(storyKey) {
            var story = epic.stories[storyKey];
            var comment = commentsPerStory[storyKey];

            if (!comment) {
                return;
            }

            var res = comment.match(/([\s\S]*)\{demo\}([\s\S]*)\{\/demo\}([\s\S]*)/);
            if (res) {
                story.commentTitle = res[2];
                story.comment = res[1] + res[3];
            } else {
                res = comment.match(/([\s\S]*)\{demo\}([\s\S]*)/);
                if (res) {
                    story.commentTitle = story.title;
                    story.comment = res[1] + res[2];
                } else {
                    res = comment.match(/([\s\S]*)#demo([\s\S]*)/);
                    if (res) {
                        story.commentTitle = story.title;
                        story.comment = res[1] + res[2];
                    }
                }
            }
        });
    });

    return epics;
}

function enrichStoriesWithComments(epics) {
    var storyKeys = Object.keys(epics).reduce(function(storyKeys, epicKey) {
        var epic = epics[epicKey];

        return storyKeys.concat(Object.keys(epic.stories));
    }, []);

    return jiraWrapper.retrieveCommentsFromStories(storyKeys).then(function(commentCollections) {
        return associateCommentsToStories(commentCollections, storyKeys, epics);
    });
}

app.get('/v3/:number([0-9]+)', function(req, res) {
    var debug = typeof req.query.debug !== 'undefined';
    var sprint = req.params.number;
    jiraWrapper.retrieveStoriesFromSprint(sprint, 'ForumSquad Sprint')
    .then(function(res) {
        var stories = filterStories(res.issues, debug);
        return storiesToEpicsStructure(stories);
    })
    .then(getEpicsTitles)
    .then(enrichStoriesWithComments)
    .then(function(epics) {
        res.render('v3.html', { epics: epics, sprint: sprint });
    });
});







app.listen(port);
console.log('Express started on http://localhost:' + port);
