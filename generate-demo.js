#!/usr/bin/env node

var yeoman = require('yeoman-environment');
var env = yeoman.createEnv();

//env.register(require.resolve('./generators/app/index'));
env.registerStub(require('./generators/app/index'), 'npm:app');
env.run('npm:app');