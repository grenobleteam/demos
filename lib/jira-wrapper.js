/**
 * Created by loic on 27/07/15.
 */

var Q = require('q');
var request = require('request');
var _ = require('lodash');
var slug = require('slug');

var apiUrl = 'https://purch1.atlassian.net/rest/api/2/search';
var EPICID_FIELD = 'customfield_10007';
var EPICNAME_FIELD = 'customfield_10008';
var ESTIMATE_FIELD = 'customfield_10004';
var STATUS_FIELD = 'status';
var DESCRIPTION_FIELD = 'description';
var TITLE_FIELD = 'summary';
var EPICNAME_FIELD = 'project';

module.exports = function(user, password) {
    var auth = {
        user: user,
        password: password
    };
    return {
        retrieveStoriesFromSprint: function (sprint, sprintName) {
            sprintName = sprintName || 'MediaSprints Sprint';
            var d = Q.defer();

            var data = {
                jql: 'Sprint = "' + sprintName + ' ' + sprint + '" and type != "Task" and type != "Sub-task" and ("labels" = teagre or project = GRE)',
                fields: [EPICID_FIELD, ESTIMATE_FIELD, STATUS_FIELD, TITLE_FIELD, EPICNAME_FIELD].join(','),
                maxResults: 200
            };

            request({
                url: apiUrl,
                qs: data,
                auth: auth
            }, function (err, response, body) {
                if (err) d.reject(err);

                return d.resolve(JSON.parse(body));
            });

            return d.promise;
        },

        retrieveEpicsFromStories: function (stories) {
            var d = Q.defer();

            var epicIds = _.uniq(stories.map(function (currentStory) {
                return currentStory.fields[EPICID_FIELD];
            }));
            epicIds = _.compact(epicIds);

            var data = {
                jql: 'key = ' + epicIds.join(' OR key = '),
                fields: EPICNAME_FIELD
            };


            request({
                url: apiUrl,
                qs: data,
                auth: auth
            }, function(err, response, body) {
                if (err) d.reject(err);

                var json = JSON.parse(body);

                var epics = json.issues.reduce(function (epics, currentEpic) {
                    epics[currentEpic.key] = {
                        name: currentEpic.fields[EPICNAME_FIELD],
                        slug: slug(currentEpic.fields[EPICNAME_FIELD], {lower: true})
                    };

                    return epics;
                }, {});

                d.resolve(epics);
            });

            return d.promise;
        },

        retrieveStories: function(storyKeys) {
            var d = Q.defer();

            var data = {
                jql: 'key = ' + storyKeys.join(' OR key = '),
                fields: [TITLE_FIELD].join(',')
            };

            request({
                url: apiUrl,
                qs: data,
                auth: auth
            }, function(err, response, body) {
                if (err) d.reject(err);

                var json = JSON.parse(body);

                d.resolve(json.issues);
            });

            return d.promise;
        },

        retrieveCommentsFromStories: function(storyKeys) {
            var d = Q.defer();

            var promises = storyKeys.map(function(storyKey) {
                return new Promise(function(resolve, reject) {
                    request({
                        url: 'https://purch1.atlassian.net/rest/api/2/issue/' + storyKey + '/comment',
                        qs: {
                            expand: 'renderedBody'
                        },
                        auth: auth
                    }, function(err, response, body) {
                        if (err) reject(err);

                        var json = JSON.parse(body);

                        if (json.total == 0) {
                            return resolve([]);
                        }

                        resolve(json.comments);
                    });
                });
            });

            return Promise.all(promises);
        }
    }
};